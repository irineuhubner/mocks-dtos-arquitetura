package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;
import java.util.List;

public class MovimentacaoInvestimentoDTO {
    private Integer id;
    private String nome;
    private String descricao;
    private BigDecimal taf;
    private String cnpj;
    private BigDecimal cotasParticipante;
    private BigDecimal cotasEmpresa;
    private BigDecimal valorParticipante;
    private BigDecimal valorEmpresa;
    private BigDecimal valorTotal;

    private List<MovimentacaoEncargoDTO> listaEncargos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getTaf() {
        return taf;
    }

    public void setTaf(BigDecimal taf) {
        this.taf = taf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getCotasParticipante() {
        return cotasParticipante;
    }

    public void setCotasParticipante(BigDecimal cotasParticipante) {
        this.cotasParticipante = cotasParticipante;
    }

    public BigDecimal getCotasEmpresa() {
        return cotasEmpresa;
    }

    public void setCotasEmpresa(BigDecimal cotasEmpresa) {
        this.cotasEmpresa = cotasEmpresa;
    }

    public BigDecimal getValorParticipante() {
        return valorParticipante;
    }

    public void setValorParticipante(BigDecimal valorParticipante) {
        this.valorParticipante = valorParticipante;
    }

    public BigDecimal getValorEmpresa() {
        return valorEmpresa;
    }

    public void setValorEmpresa(BigDecimal valorEmpresa) {
        this.valorEmpresa = valorEmpresa;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public List<MovimentacaoEncargoDTO> getListaEncargos() {
        return listaEncargos;
    }

    public void setListaEncargos(List<MovimentacaoEncargoDTO> listaEncargos) {
        this.listaEncargos = listaEncargos;
    }
}
