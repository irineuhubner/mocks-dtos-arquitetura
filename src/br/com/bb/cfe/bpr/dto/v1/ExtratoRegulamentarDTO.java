package br.com.bb.cfe.bpr.dto.v1;

public class ExtratoRegulamentarDTO {
    private String status;
    private byte[] file;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }
}
