package br.com.bb.cfe.bpr.dto.v1;

public class DadosContratuaisDTO {
    private String tipoTributacao;
    private String tipoPlano;

    private DadosContratuaisOpcaoRendaDTO opcaoRenda;

    public String getTipoTributacao() {
        return tipoTributacao;
    }

    public void setTipoTributacao(String tipoTributacao) {
        this.tipoTributacao = tipoTributacao;
    }

    public String getTipoPlano() {
        return tipoPlano;
    }

    public void setTipoPlano(String tipoPlano) {
        this.tipoPlano = tipoPlano;
    }

    public DadosContratuaisOpcaoRendaDTO getOpcaoRenda() {
        return opcaoRenda;
    }

    public void setOpcaoRenda(DadosContratuaisOpcaoRendaDTO opcaoRenda) {
        this.opcaoRenda = opcaoRenda;
    }
}
