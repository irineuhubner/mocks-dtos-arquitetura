package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class RendimentoConsolidadoDTO {
    private BigDecimal valorTotalRendimentos;
    private BigDecimal valorRendimentosParticipante;
    private BigDecimal valorRendimentosEmpresa;

    public BigDecimal getValorTotalRendimentos() {
        return valorTotalRendimentos;
    }

    public void setValorTotalRendimentos(BigDecimal valorTotalRendimentos) {
        this.valorTotalRendimentos = valorTotalRendimentos;
    }

    public BigDecimal getValorRendimentosParticipante() {
        return valorRendimentosParticipante;
    }

    public void setValorRendimentosParticipante(BigDecimal valorRendimentosParticipante) {
        this.valorRendimentosParticipante = valorRendimentosParticipante;
    }

    public BigDecimal getValorRendimentosEmpresa() {
        return valorRendimentosEmpresa;
    }

    public void setValorRendimentosEmpresa(BigDecimal valorRendimentosEmpresa) {
        this.valorRendimentosEmpresa = valorRendimentosEmpresa;
    }
}