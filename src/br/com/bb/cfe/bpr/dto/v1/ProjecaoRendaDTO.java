package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class ProjecaoRendaDTO {
    private BigDecimal beneficioAtual;
    private BigDecimal beneficioSimulado;
    private BigDecimal beneficioIdeal;
    private BigDecimal percentualContinuidade;
    private BigDecimal fatorAtuarialConcessao;
    private BigDecimal contribuicaoParticipante;

    public BigDecimal getBeneficioAtual() {
        return beneficioAtual;
    }

    public void setBeneficioAtual(BigDecimal beneficioAtual) {
        this.beneficioAtual = beneficioAtual;
    }

    public BigDecimal getBeneficioSimulado() {
        return beneficioSimulado;
    }

    public void setBeneficioSimulado(BigDecimal beneficioSimulado) {
        this.beneficioSimulado = beneficioSimulado;
    }

    public BigDecimal getBeneficioIdeal() {
        return beneficioIdeal;
    }

    public void setBeneficioIdeal(BigDecimal beneficioIdeal) {
        this.beneficioIdeal = beneficioIdeal;
    }

    public BigDecimal getPercentualContinuidade() {
        return percentualContinuidade;
    }

    public void setPercentualContinuidade(BigDecimal percentualContinuidade) {
        this.percentualContinuidade = percentualContinuidade;
    }

    public BigDecimal getFatorAtuarialConcessao() {
        return fatorAtuarialConcessao;
    }

    public void setFatorAtuarialConcessao(BigDecimal fatorAtuarialConcessao) {
        this.fatorAtuarialConcessao = fatorAtuarialConcessao;
    }

    public BigDecimal getContribuicaoParticipante() {
        return contribuicaoParticipante;
    }

    public void setContribuicaoParticipante(BigDecimal contribuicaoParticipante) {
        this.contribuicaoParticipante = contribuicaoParticipante;
    }
}
