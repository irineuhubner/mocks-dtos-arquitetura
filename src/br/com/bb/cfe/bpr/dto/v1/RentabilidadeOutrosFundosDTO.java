package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class RentabilidadeOutrosFundosDTO {
    private Integer id;
    private String nome;
    private String descricao;
    private BigDecimal taf;
    private String cnpj;
    private BigDecimal percentualAcumuladoAno;
    private BigDecimal percentualAcumulado12;
    private BigDecimal percentualAcumulado36;
    private BigDecimal percentualAcumulado60;
    private BigDecimal riscoGrau;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getTaf() {
        return taf;
    }

    public void setTaf(BigDecimal taf) {
        this.taf = taf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getPercentualAcumuladoAno() {
        return percentualAcumuladoAno;
    }

    public void setPercentualAcumuladoAno(BigDecimal percentualAcumuladoAno) {
        this.percentualAcumuladoAno = percentualAcumuladoAno;
    }

    public BigDecimal getPercentualAcumulado12() {
        return percentualAcumulado12;
    }

    public void setPercentualAcumulado12(BigDecimal percentualAcumulado12) {
        this.percentualAcumulado12 = percentualAcumulado12;
    }

    public BigDecimal getPercentualAcumulado36() {
        return percentualAcumulado36;
    }

    public void setPercentualAcumulado36(BigDecimal percentualAcumulado36) {
        this.percentualAcumulado36 = percentualAcumulado36;
    }

    public BigDecimal getPercentualAcumulado60() {
        return percentualAcumulado60;
    }

    public void setPercentualAcumulado60(BigDecimal percentualAcumulado60) {
        this.percentualAcumulado60 = percentualAcumulado60;
    }

    public BigDecimal getRiscoGrau() {
        return riscoGrau;
    }

    public void setRiscoGrau(BigDecimal riscoGrau) {
        this.riscoGrau = riscoGrau;
    }
}
