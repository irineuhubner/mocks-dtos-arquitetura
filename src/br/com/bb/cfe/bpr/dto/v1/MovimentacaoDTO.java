package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;
import java.util.List;

public class MovimentacaoDTO {
    private Integer id;
    private String nome;
    private String data;
    private BigDecimal total;
    private BigDecimal totalParticipante;
    private BigDecimal totalEmpresa;
    private BigDecimal encargos;
    private BigDecimal encargosParticipante;
    private BigDecimal encargosEmpresa;

    private List<MovimentacaoInvestimentoDTO> listaInvestimentos;
    private List<MovimentacaoRiscoDTO> listaRiscos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getTotalParticipante() {
        return totalParticipante;
    }

    public void setTotalParticipante(BigDecimal totalParticipante) {
        this.totalParticipante = totalParticipante;
    }

    public BigDecimal getTotalEmpresa() {
        return totalEmpresa;
    }

    public void setTotalEmpresa(BigDecimal totalEmpresa) {
        this.totalEmpresa = totalEmpresa;
    }

    public BigDecimal getEncargos() {
        return encargos;
    }

    public void setEncargos(BigDecimal encargos) {
        this.encargos = encargos;
    }

    public BigDecimal getEncargosParticipante() {
        return encargosParticipante;
    }

    public void setEncargosParticipante(BigDecimal encargosParticipante) {
        this.encargosParticipante = encargosParticipante;
    }

    public BigDecimal getEncargosEmpresa() {
        return encargosEmpresa;
    }

    public void setEncargosEmpresa(BigDecimal encargosEmpresa) {
        this.encargosEmpresa = encargosEmpresa;
    }

    public List<MovimentacaoInvestimentoDTO> getListaInvestimentos() {
        return listaInvestimentos;
    }

    public void setListaInvestimentos(List<MovimentacaoInvestimentoDTO> listaInvestimentos) {
        this.listaInvestimentos = listaInvestimentos;
    }

    public List<MovimentacaoRiscoDTO> getListaRiscos() {
        return listaRiscos;
    }

    public void setListaRiscos(List<MovimentacaoRiscoDTO> listaRiscos) {
        this.listaRiscos = listaRiscos;
    }
}
