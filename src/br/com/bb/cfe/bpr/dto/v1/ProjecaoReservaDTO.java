package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class ProjecaoReservaDTO {
    private BigDecimal reservaParticipante;
    private BigDecimal rendimentosReservaParticipante;
    private BigDecimal contribuicoesReservaParticipante;
    private BigDecimal contribuicaoParticipante;

    public BigDecimal getReservaParticipante() {
        return reservaParticipante;
    }

    public void setReservaParticipante(BigDecimal reservaParticipante) {
        this.reservaParticipante = reservaParticipante;
    }

    public BigDecimal getRendimentosReservaParticipante() {
        return rendimentosReservaParticipante;
    }

    public void setRendimentosReservaParticipante(BigDecimal rendimentosReservaParticipante) {
        this.rendimentosReservaParticipante = rendimentosReservaParticipante;
    }

    public BigDecimal getContribuicoesReservaParticipante() {
        return contribuicoesReservaParticipante;
    }

    public void setContribuicoesReservaParticipante(BigDecimal contribuicoesReservaParticipante) {
        this.contribuicoesReservaParticipante = contribuicoesReservaParticipante;
    }

    public BigDecimal getContribuicaoParticipante() {
        return contribuicaoParticipante;
    }

    public void setContribuicaoParticipante(BigDecimal contribuicaoParticipante) {
        this.contribuicaoParticipante = contribuicaoParticipante;
    }
}
