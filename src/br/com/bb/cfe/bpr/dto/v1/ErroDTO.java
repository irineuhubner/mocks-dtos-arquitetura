package br.com.bb.cfe.bpr.dto.v1;

public class ErroDTO {
    private Integer codigoErro;
    private String nomeErro;
    private String descricaoErro;

    public Integer getCodigoErro() {
        return codigoErro;
    }

    public void setCodigoErro(Integer codigoErro) {
        this.codigoErro = codigoErro;
    }

    public String getNomeErro() {
        return nomeErro;
    }

    public void setNomeErro(String nomeErro) {
        this.nomeErro = nomeErro;
    }

    public String getDescricaoErro() {
        return descricaoErro;
    }

    public void setDescricaoErro(String descricaoErro) {
        this.descricaoErro = descricaoErro;
    }
}
