package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class BeneficioRiscoDTO {
    private Integer id;
    private String nome;
    private BigDecimal premio;
    private BigDecimal valorParticipante;
    private BigDecimal valorEmpresa;
    private String susep;
    private String periodicidadeContribuicao;
    private String status;
    private String inicioVigencia;
    private String fimVigencia;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public BigDecimal getValorParticipante() {
        return valorParticipante;
    }

    public void setValorParticipante(BigDecimal valorParticipante) {
        this.valorParticipante = valorParticipante;
    }

    public BigDecimal getValorEmpresa() {
        return valorEmpresa;
    }

    public void setValorEmpresa(BigDecimal valorEmpresa) {
        this.valorEmpresa = valorEmpresa;
    }

    public String getSusep() {
        return susep;
    }

    public void setSusep(String susep) {
        this.susep = susep;
    }

    public String getPeriodicidadeContribuicao() {
        return periodicidadeContribuicao;
    }

    public void setPeriodicidadeContribuicao(String periodicidadeContribuicao) {
        this.periodicidadeContribuicao = periodicidadeContribuicao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(String inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public String getFimVigencia() {
        return fimVigencia;
    }

    public void setFimVigencia(String fimVigencia) {
        this.fimVigencia = fimVigencia;
    }
}
