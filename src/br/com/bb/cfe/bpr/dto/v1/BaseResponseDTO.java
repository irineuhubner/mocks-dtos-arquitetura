package br.com.bb.cfe.bpr.dto.v1;

import java.io.Serializable;
import java.util.List;

public class BaseResponseDTO<T> implements Serializable {
    private String status;
    private T data;
    private List<ErroDTO> errors;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<ErroDTO> getErrors() {
        return errors;
    }

    public void setErrors(List<ErroDTO> errors) {
        this.errors = errors;
    }
}