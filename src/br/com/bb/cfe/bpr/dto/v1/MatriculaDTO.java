package br.com.bb.cfe.bpr.dto.v1;

public class MatriculaDTO {
    private String matricula;
    private String dataInscricao;
    private String tipoPlano;
    private String nomePlano;
    private Integer idTipoContratacao;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getDataInscricao() {
        return dataInscricao;
    }

    public void setDataInscricao(String dataInscricao) {
        this.dataInscricao = dataInscricao;
    }

    public String getTipoPlano() {
        return tipoPlano;
    }

    public void setTipoPlano(String tipoPlano) {
        this.tipoPlano = tipoPlano;
    }

    public String getNomePlano() {
        return nomePlano;
    }

    public void setNomePlano(String nomePlano) {
        this.nomePlano = nomePlano;
    }

    public Integer getIdTipoContratacao() {
        return idTipoContratacao;
    }

    public void setIdTipoContratacao(Integer idTipoContratacao) {
        this.idTipoContratacao = idTipoContratacao;
    }
}
