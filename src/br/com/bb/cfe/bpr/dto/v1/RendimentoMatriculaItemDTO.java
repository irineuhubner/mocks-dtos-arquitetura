package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class RendimentoMatriculaItemDTO {
    private Integer id;
    private String nome;
    private String descricao;
    private BigDecimal taf;
    private String cnpj;
    private BigDecimal rendimentoParticipante;
    private BigDecimal rendimentoEmpresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getTaf() {
        return taf;
    }

    public void setTaf(BigDecimal taf) {
        this.taf = taf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getRendimentoParticipante() {
        return rendimentoParticipante;
    }

    public void setRendimentoParticipante(BigDecimal rendimentoParticipante) {
        this.rendimentoParticipante = rendimentoParticipante;
    }

    public BigDecimal getRendimentoEmpresa() {
        return rendimentoEmpresa;
    }

    public void setRendimentoEmpresa(BigDecimal rendimentoEmpresa) {
        this.rendimentoEmpresa = rendimentoEmpresa;
    }
}
