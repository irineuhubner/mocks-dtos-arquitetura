package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class SaldoMatriculaInvestimentoDTO {
    private Integer id;
    private String nome;
    private String descricao;
    private BigDecimal taf;
    private String cnpj;
    private BigDecimal cotasParticipante;
    private BigDecimal cotasEmpresa;
    private BigDecimal lancamentoParticipante;
    private BigDecimal lancamentoEmpresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getTaf() {
        return taf;
    }

    public void setTaf(BigDecimal taf) {
        this.taf = taf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getCotasParticipante() {
        return cotasParticipante;
    }

    public void setCotasParticipante(BigDecimal cotasParticipante) {
        this.cotasParticipante = cotasParticipante;
    }

    public BigDecimal getCotasEmpresa() {
        return cotasEmpresa;
    }

    public void setCotasEmpresa(BigDecimal cotasEmpresa) {
        this.cotasEmpresa = cotasEmpresa;
    }

    public BigDecimal getLancamentoParticipante() {
        return lancamentoParticipante;
    }

    public void setLancamentoParticipante(BigDecimal lancamentoParticipante) {
        this.lancamentoParticipante = lancamentoParticipante;
    }

    public BigDecimal getLancamentoEmpresa() {
        return lancamentoEmpresa;
    }

    public void setLancamentoEmpresa(BigDecimal lancamentoEmpresa) {
        this.lancamentoEmpresa = lancamentoEmpresa;
    }
}
