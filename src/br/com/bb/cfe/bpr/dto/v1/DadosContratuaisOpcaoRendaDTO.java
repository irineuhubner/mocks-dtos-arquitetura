package br.com.bb.cfe.bpr.dto.v1;

public class DadosContratuaisOpcaoRendaDTO {
    private Integer id;
    private String tipo;
    private Integer temporariedade;
    private String dataConcessao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getTemporariedade() {
        return temporariedade;
    }

    public void setTemporariedade(Integer temporariedade) {
        this.temporariedade = temporariedade;
    }

    public String getDataConcessao() {
        return dataConcessao;
    }

    public void setDataConcessao(String dataConcessao) {
        this.dataConcessao = dataConcessao;
    }
}
