package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;
import java.util.List;

public class SaldoMatriculaDTO {
    private BigDecimal totalLancamentos;
    private BigDecimal totalLancamentosParticipante;
    private BigDecimal totalLancamentosEmpresa;
    private BigDecimal tafMedia;

    private List<SaldoMatriculaInvestimentoDTO> listaInvestimentos;

    public BigDecimal getTotalLancamentos() {
        return totalLancamentos;
    }

    public void setTotalLancamentos(BigDecimal totalLancamentos) {
        this.totalLancamentos = totalLancamentos;
    }

    public BigDecimal getTotalLancamentosParticipante() {
        return totalLancamentosParticipante;
    }

    public void setTotalLancamentosParticipante(BigDecimal totalLancamentosParticipante) {
        this.totalLancamentosParticipante = totalLancamentosParticipante;
    }

    public BigDecimal getTotalLancamentosEmpresa() {
        return totalLancamentosEmpresa;
    }

    public void setTotalLancamentosEmpresa(BigDecimal totalLancamentosEmpresa) {
        this.totalLancamentosEmpresa = totalLancamentosEmpresa;
    }

    public BigDecimal getTafMedia() {
        return tafMedia;
    }

    public void setTafMedia(BigDecimal tafMedia) {
        this.tafMedia = tafMedia;
    }

    public List<SaldoMatriculaInvestimentoDTO> getListaInvestimentos() {
        return listaInvestimentos;
    }

    public void setListaInvestimentos(List<SaldoMatriculaInvestimentoDTO> listaInvestimentos) {
        this.listaInvestimentos = listaInvestimentos;
    }
}
