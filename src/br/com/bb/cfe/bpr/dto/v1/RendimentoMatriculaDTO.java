package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;
import java.util.List;

public class RendimentoMatriculaDTO {
    private BigDecimal totalRendimentos;

    private List<RendimentoMatriculaItemDTO> listaInvestimentos;

    public BigDecimal getTotalRendimentos() {
        return totalRendimentos;
    }

    public void setTotalRendimentos(BigDecimal totalRendimentos) {
        this.totalRendimentos = totalRendimentos;
    }

    public List<RendimentoMatriculaItemDTO> getListaInvestimentos() {
        return listaInvestimentos;
    }

    public void setListaInvestimentos(List<RendimentoMatriculaItemDTO> listaInvestimentos) {
        this.listaInvestimentos = listaInvestimentos;
    }
}
