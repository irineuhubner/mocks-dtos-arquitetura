package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class SaldoConsolidadoDTO {
    private BigDecimal valorTotalLancamentos;
    private BigDecimal valorLancamentosParticipante;
    private BigDecimal valorLancamentosEmpresa;

    public BigDecimal getValorTotalLancamentos() {
        return valorTotalLancamentos;
    }

    public void setValorTotalLancamentos(BigDecimal valorTotalLancamentos) {
        this.valorTotalLancamentos = valorTotalLancamentos;
    }

    public BigDecimal getValorLancamentosParticipante() {
        return valorLancamentosParticipante;
    }

    public void setValorLancamentosParticipante(BigDecimal valorLancamentosParticipante) {
        this.valorLancamentosParticipante = valorLancamentosParticipante;
    }

    public BigDecimal getValorLancamentosEmpresa() {
        return valorLancamentosEmpresa;
    }

    public void setValorLancamentosEmpresa(BigDecimal valorLancamentosEmpresa) {
        this.valorLancamentosEmpresa = valorLancamentosEmpresa;
    }
}