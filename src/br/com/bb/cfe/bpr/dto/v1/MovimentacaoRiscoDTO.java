package br.com.bb.cfe.bpr.dto.v1;

import java.math.BigDecimal;

public class MovimentacaoRiscoDTO {
    private Integer id;
    private String nome;
    private BigDecimal valorParticipante;
    private BigDecimal valorEmpresa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getValorParticipante() {
        return valorParticipante;
    }

    public void setValorParticipante(BigDecimal valorParticipante) {
        this.valorParticipante = valorParticipante;
    }

    public BigDecimal getValorEmpresa() {
        return valorEmpresa;
    }

    public void setValorEmpresa(BigDecimal valorEmpresa) {
        this.valorEmpresa = valorEmpresa;
    }
}
